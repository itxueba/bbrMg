#检查当前已安装内核，并删除新内核以外的内核
rpm -qa | grep kernel


#安装魔改BBR

#安装依赖
yum -y install make gcc
#安装魔改BBR
echo "obj-m:=tcp_tsunami.o" > Makefile
make -C /lib/modules/`uname -r`/build M=`pwd` modules CC=/usr/bin/gcc
chmod +x ./tcp_tsunami.ko
cp -rf ./tcp_tsunami.ko /lib/modules/`uname -r`/kernel/net/ipv4
insmod tcp_tsunami.ko
depmod -a
#修改配置
echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
echo "net.ipv4.tcp_congestion_control=tsunami" >> /etc/sysctl.conf
sysctl -p

#检查开启状态
lsmod | grep tsunami
