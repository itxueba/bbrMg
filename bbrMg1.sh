#CentOS 7系统
#下载并安装内核
yum -y install kernel-ml-4.12.10-1.el7.elrepo.x86_64.rpm
yum -y install kernel-ml-devel-4.12.10-1.el7.elrepo.x86_64.rpm
yum -y install kernel-ml-headers-4.12.10-1.el7.elrepo.x86_64.rpm
#调整内核启动顺序
grub2-mkconfig -o /boot/grub2/grub.cfg && grub2-set-default 0
#重启
shutdown -r now


